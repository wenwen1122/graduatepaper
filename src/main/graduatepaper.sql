/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : graduatepaper

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2021-10-02 11:02:49
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `tb_depart`
-- ----------------------------
DROP TABLE IF EXISTS `tb_depart`;
CREATE TABLE `tb_depart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departname` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '专业名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_depart
-- ----------------------------
INSERT INTO tb_depart VALUES ('1', '计算机学院');
INSERT INTO tb_depart VALUES ('2', '城市建设学院');
INSERT INTO tb_depart VALUES ('3', '数学与统计学院');
INSERT INTO tb_depart VALUES ('4', '化学化工学院');
INSERT INTO tb_depart VALUES ('5', '人文与新闻传播学院');
INSERT INTO tb_depart VALUES ('6', '教师教育学院');
INSERT INTO tb_depart VALUES ('7', '物理与电子信息工程学院');
INSERT INTO tb_depart VALUES ('8', '外国语学院');
INSERT INTO tb_depart VALUES ('9', '机电工程学院');
INSERT INTO tb_depart VALUES ('10', '音乐与舞蹈学院');
INSERT INTO tb_depart VALUES ('11', '美术与设计学院');
INSERT INTO tb_depart VALUES ('12', '生物工程学院');
INSERT INTO tb_depart VALUES ('13', '政法学院');
INSERT INTO tb_depart VALUES ('14', '体育与健康学院');
INSERT INTO tb_depart VALUES ('15', '牡丹学院');
INSERT INTO tb_depart VALUES ('16', '药学院');

-- ----------------------------
-- Table structure for `tb_paper`
-- ----------------------------
DROP TABLE IF EXISTS `tb_paper`;
CREATE TABLE `tb_paper` (
  `paper_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL COMMENT '提交论文的学生ID',
  `plan_id` int(10) unsigned NOT NULL COMMENT '本次论文提交对应的执行计划',
  `teacher_id` int(10) unsigned NOT NULL COMMENT '应该负责审阅本次论文的负责教师',
  `check_state` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0表示未审批，双方可见。1表示已经审批，教师学生可见。2表示已经审批教师可见，学生不可见，3表示已经审批，学生可见，教师不可见',
  `student_tol` varchar(600) COLLATE utf8_bin NOT NULL DEFAULT '无' COMMENT '学生对论文的简单介绍',
  `teacher_tol` varchar(600) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '教师评语',
  `paper_path` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '论文保存相对路径',
  `pass_state` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0表示论文需要修改，尚未通过。1表示论文已经通过',
  `submit_time` datetime NOT NULL COMMENT '论文提交时间',
  `judge_time` datetime NOT NULL COMMENT '论文审批时间',
  PRIMARY KEY (`paper_id`),
  KEY `teacher_index` (`teacher_id`),
  KEY `state_index` (`check_state`),
  KEY `student_index` (`student_id`),
  KEY `pass_index` (`pass_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_paper
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_plan`
-- ----------------------------
DROP TABLE IF EXISTS `tb_plan`;
CREATE TABLE `tb_plan` (
  `plan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL COMMENT '教师ID',
  `plan_content` varchar(160) COLLATE utf8_bin NOT NULL COMMENT '计划JSON字符串',
  PRIMARY KEY (`plan_id`),
  UNIQUE KEY `teacher_index` (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_plan
-- ----------------------------
INSERT INTO tb_plan VALUES ('1', '2', '计划书');
INSERT INTO tb_plan VALUES ('2', '2', '开题报告');
INSERT INTO tb_plan VALUES ('3', '2', '中期审查');
INSERT INTO tb_plan VALUES ('4', '2', '答辩稿');

-- ----------------------------
-- Table structure for `tb_profess`
-- ----------------------------
DROP TABLE IF EXISTS `tb_profess`;
CREATE TABLE `tb_profess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `professname` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '专业名称',
  `departid` int(11) NOT NULL COMMENT '所属院系id',
  PRIMARY KEY (`id`),
  KEY `depart_index` (`departid`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='专业名称表';

-- ----------------------------
-- Records of tb_profess
-- ----------------------------
INSERT INTO tb_profess VALUES ('1', '计算机科学与技术', '1');
INSERT INTO tb_profess VALUES ('2', '软件开发', '1');
INSERT INTO tb_profess VALUES ('3', '软件测试', '1');
INSERT INTO tb_profess VALUES ('4', '云计算', '1');
INSERT INTO tb_profess VALUES ('5', '3D仿真', '1');
INSERT INTO tb_profess VALUES ('6', '土木工程', '2');
INSERT INTO tb_profess VALUES ('7', '建筑工程', '2');
INSERT INTO tb_profess VALUES ('8', '城乡规划', '2');
INSERT INTO tb_profess VALUES ('9', '建筑学', '2');
INSERT INTO tb_profess VALUES ('10', '人文地理', '2');
INSERT INTO tb_profess VALUES ('11', '理论数学', '3');
INSERT INTO tb_profess VALUES ('12', '应用数学', '3');
INSERT INTO tb_profess VALUES ('13', '统计学', '3');
INSERT INTO tb_profess VALUES ('14', '信息与计算科学', '3');
INSERT INTO tb_profess VALUES ('15', '无机化学', '4');
INSERT INTO tb_profess VALUES ('16', '有机化学', '4');
INSERT INTO tb_profess VALUES ('17', '分析化学', '4');
INSERT INTO tb_profess VALUES ('18', '高分子化学与物理', '4');
INSERT INTO tb_profess VALUES ('19', '物理化学', '4');
INSERT INTO tb_profess VALUES ('20', '新闻学', '5');
INSERT INTO tb_profess VALUES ('21', '广播电视学', '5');
INSERT INTO tb_profess VALUES ('22', '汉语言文学', '5');
INSERT INTO tb_profess VALUES ('23', '播音主持', '5');
INSERT INTO tb_profess VALUES ('24', '广告学', '5');
INSERT INTO tb_profess VALUES ('25', '小学教育', '6');
INSERT INTO tb_profess VALUES ('26', '学前教育', '6');
INSERT INTO tb_profess VALUES ('27', '学科教育类', '6');
INSERT INTO tb_profess VALUES ('28', '物理学', '7');
INSERT INTO tb_profess VALUES ('29', '电子信息科学与技术', '7');
INSERT INTO tb_profess VALUES ('30', '电气自动化', '7');
INSERT INTO tb_profess VALUES ('31', '应用电子技术', '7');
INSERT INTO tb_profess VALUES ('32', '俄罗斯语', '8');
INSERT INTO tb_profess VALUES ('33', '日本语', '8');
INSERT INTO tb_profess VALUES ('34', '商务英语', '8');
INSERT INTO tb_profess VALUES ('35', '韩语', '8');
INSERT INTO tb_profess VALUES ('36', '西班牙语', '8');
INSERT INTO tb_profess VALUES ('37', '机械电子工程', '9');
INSERT INTO tb_profess VALUES ('38', '自动化', '9');
INSERT INTO tb_profess VALUES ('39', '机电一体化', '9');
INSERT INTO tb_profess VALUES ('40', '音乐学', '10');
INSERT INTO tb_profess VALUES ('41', '舞蹈学', '10');
INSERT INTO tb_profess VALUES ('42', '音乐表演', '10');
INSERT INTO tb_profess VALUES ('43', '平面设计', '11');
INSERT INTO tb_profess VALUES ('44', '动画设计', '11');
INSERT INTO tb_profess VALUES ('45', '视觉传达', '11');
INSERT INTO tb_profess VALUES ('46', '服装设计', '11');
INSERT INTO tb_profess VALUES ('47', '动物学', '12');
INSERT INTO tb_profess VALUES ('48', '植物学', '12');
INSERT INTO tb_profess VALUES ('49', '现代生物技术', '12');
INSERT INTO tb_profess VALUES ('50', '遗传学', '12');
INSERT INTO tb_profess VALUES ('51', '微生物学', '12');
INSERT INTO tb_profess VALUES ('52', '法学', '13');
INSERT INTO tb_profess VALUES ('53', '法学理论', '13');
INSERT INTO tb_profess VALUES ('54', '刑法学', '13');
INSERT INTO tb_profess VALUES ('55', '商务法律', '13');
INSERT INTO tb_profess VALUES ('56', '诉讼学', '13');
INSERT INTO tb_profess VALUES ('57', '体育教育', '14');
INSERT INTO tb_profess VALUES ('58', '竞技体育', '14');
INSERT INTO tb_profess VALUES ('59', '风景园林', '15');
INSERT INTO tb_profess VALUES ('60', '农业资源与环境', '15');
INSERT INTO tb_profess VALUES ('61', '植物科学与技术', '15');
INSERT INTO tb_profess VALUES ('62', '植物保护', '15');
INSERT INTO tb_profess VALUES ('63', '药剂学', '16');
INSERT INTO tb_profess VALUES ('64', '中药学', '16');
INSERT INTO tb_profess VALUES ('65', '应用药学', '16');

-- ----------------------------
-- Table structure for `tb_student`
-- ----------------------------
DROP TABLE IF EXISTS `tb_student`;
CREATE TABLE `tb_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '学生在user表中的id',
  `profeid` int(11) NOT NULL DEFAULT '7' COMMENT '学生专业',
  `check_title` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0表示没有选题，非零表示选题id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `student_user` (`userid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_student
-- ----------------------------
INSERT INTO tb_student VALUES ('1', '1', '1', '1');

-- ----------------------------
-- Table structure for `tb_teacher`
-- ----------------------------
DROP TABLE IF EXISTS `tb_teacher`;
CREATE TABLE `tb_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL COMMENT '教师用户在user表中的id',
  `protitle` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '职称或者称号',
  `direction` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '研究方向',
  PRIMARY KEY (`id`),
  KEY `teacher_user` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_teacher
-- ----------------------------
INSERT INTO tb_teacher VALUES ('1', '2', '正教授', '软件理论分析');

-- ----------------------------
-- Table structure for `tb_title`
-- ----------------------------
DROP TABLE IF EXISTS `tb_title`;
CREATE TABLE `tb_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_name` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '题目名称',
  `title_description` varchar(300) COLLATE utf8_bin NOT NULL COMMENT '对论文题目的简单介绍',
  `teacher_id` int(11) NOT NULL,
  `publish_time` datetime NOT NULL,
  `limit_time` datetime NOT NULL COMMENT '截止时间',
  `depart_id` int(11) NOT NULL COMMENT '归属院系',
  `limit_stu` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '限制选题学生人数',
  `check_stu` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '已经选择该题目的学生数量',
  PRIMARY KEY (`title_id`),
  KEY `teacher_index` (`teacher_id`),
  KEY `depart_index` (`depart_id`),
  KEY `starttime_index` (`publish_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_title
-- ----------------------------
INSERT INTO tb_title VALUES ('1', '前后端分离的文件收集系统', '基于Java或者Python，前后端分离，并完成接口文档以及数据字典的编写', '2', '2021-10-02 10:23:07', '2022-10-30 10:22:00', '1', '10', '1');
INSERT INTO tb_title VALUES ('2', 'JavaScript的坦克大战小游戏', '注意文档编写，并将游戏发布到公网', '2', '2021-10-02 10:24:57', '2022-10-30 10:24:00', '1', '10', '0');

-- ----------------------------
-- Table structure for `tb_title_profess`
-- ----------------------------
DROP TABLE IF EXISTS `tb_title_profess`;
CREATE TABLE `tb_title_profess` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profess_id` int(11) NOT NULL COMMENT '专业id',
  `title_id` int(11) NOT NULL COMMENT '题目id',
  PRIMARY KEY (`id`),
  KEY `title_index` (`title_id`),
  KEY `profess_index` (`profess_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_title_profess
-- ----------------------------
INSERT INTO tb_title_profess VALUES ('1', '1', '1');
INSERT INTO tb_title_profess VALUES ('2', '4', '1');
INSERT INTO tb_title_profess VALUES ('3', '2', '1');
INSERT INTO tb_title_profess VALUES ('4', '3', '1');
INSERT INTO tb_title_profess VALUES ('5', '5', '2');
INSERT INTO tb_title_profess VALUES ('6', '1', '2');

-- ----------------------------
-- Table structure for `tb_title_stu`
-- ----------------------------
DROP TABLE IF EXISTS `tb_title_stu`;
CREATE TABLE `tb_title_stu` (
  `title_stu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_id` int(10) unsigned NOT NULL COMMENT '题目id',
  `student_id` int(10) unsigned NOT NULL COMMENT '学生id',
  PRIMARY KEY (`title_stu_id`),
  KEY `title_index` (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_title_stu
-- ----------------------------
INSERT INTO tb_title_stu VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for `tb_user`
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schid` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '教师工号，学生学号',
  `name` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户姓名',
  `departid` int(15) NOT NULL COMMENT '所属院系',
  `discription` varchar(300) COLLATE utf8_bin DEFAULT '' COMMENT '用户自我介绍',
  `phone` bigint(11) NOT NULL COMMENT '用户手机号',
  `mail` varchar(40) COLLATE utf8_bin NOT NULL COMMENT '用户邮箱',
  `identify` int(1) NOT NULL DEFAULT '0' COMMENT '0表示学生，1表示教师',
  PRIMARY KEY (`id`),
  UNIQUE KEY `schid_index` (`schid`),
  UNIQUE KEY `phone_index` (`phone`),
  UNIQUE KEY `email_index` (`mail`),
  KEY `identity_index` (`identify`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO tb_user VALUES ('1', '2017134037', '卢斌', '1', '', '17852400187', '2695430372@qq.com', '0');
INSERT INTO tb_user VALUES ('2', '2017134033', '卢惠民', '1', '注意论文排版，合理安排时间', '13969972627', '208238924@qq.com', '1');

-- ----------------------------
-- Table structure for `tb_usersecurty`
-- ----------------------------
DROP TABLE IF EXISTS `tb_usersecurty`;
CREATE TABLE `tb_usersecurty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schid` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '学号工号，为本表账号',
  `password` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '用户密码',
  `identify` int(11) NOT NULL COMMENT '登陆者身份，0表示学生，1表示老师',
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_usersecurty
-- ----------------------------
INSERT INTO tb_usersecurty VALUES ('1', '2017134037', 'wenwen1122', '0', '1');
INSERT INTO tb_usersecurty VALUES ('2', '2017134033', 'wenwen1122', '1', '2');

-- ----------------------------
-- View structure for `vi_student`
-- ----------------------------
DROP VIEW IF EXISTS `vi_student`;
CREATE ALGORITHM=UNDEFINED DEFINER=`lubin`@`%` SQL SECURITY INVOKER VIEW `vi_student` AS select `tb_user`.`id` AS `id`,`tb_user`.`discription` AS `discription`,`tb_user`.`identify` AS `identify`,`tb_user`.`mail` AS `mail`,`tb_user`.`name` AS `name`,`tb_user`.`phone` AS `phone`,`tb_user`.`schid` AS `schid`,`tb_depart`.`departname` AS `departname`,`tb_profess`.`professname` AS `professname` from (((`tb_user` join `tb_student`) join `tb_depart`) join `tb_profess`) where ((`tb_user`.`id` = `tb_student`.`userid`) and (`tb_student`.`profeid` = `tb_profess`.`id`) and (`tb_depart`.`id` = `tb_user`.`departid`));

-- ----------------------------
-- View structure for `vi_teacher`
-- ----------------------------
DROP VIEW IF EXISTS `vi_teacher`;
CREATE ALGORITHM=UNDEFINED DEFINER=`lubin`@`%` SQL SECURITY INVOKER VIEW `vi_teacher` AS select `tb_user`.`id` AS `id`,`tb_user`.`discription` AS `discription`,`tb_user`.`identify` AS `identify`,`tb_user`.`mail` AS `mail`,`tb_user`.`name` AS `name`,`tb_user`.`phone` AS `phone`,`tb_user`.`schid` AS `schid`,`tb_depart`.`departname` AS `departname`,`tb_teacher`.`direction` AS `direction`,`tb_teacher`.`protitle` AS `protitle` from ((`tb_user` join `tb_teacher`) join `tb_depart`) where ((`tb_user`.`id` = `tb_teacher`.`userid`) and (`tb_depart`.`id` = `tb_user`.`departid`));

-- ----------------------------
-- View structure for `vi_title`
-- ----------------------------
DROP VIEW IF EXISTS `vi_title`;
CREATE ALGORITHM=UNDEFINED DEFINER=`lubin`@`%` SQL SECURITY INVOKER VIEW `vi_title` AS select `tb_profess`.`id` AS `profess_id`,`tb_profess`.`professname` AS `professname`,`tb_depart`.`id` AS `depart_id`,`tb_depart`.`departname` AS `departname`,`tb_title`.`title_id` AS `title_id`,`tb_title`.`title_name` AS `title_name`,`tb_title`.`title_description` AS `title_description`,`tb_title`.`publish_time` AS `publish_time`,`tb_title`.`limit_time` AS `limit_time`,`tb_user`.`id` AS `id`,`tb_user`.`schid` AS `schid`,`tb_user`.`name` AS `name`,`tb_user`.`departid` AS `departid`,`tb_user`.`discription` AS `discription`,`tb_user`.`phone` AS `phone`,`tb_user`.`mail` AS `mail`,`tb_user`.`identify` AS `identify`,`tb_teacher`.`protitle` AS `protitle`,`tb_teacher`.`direction` AS `direction` from (((((`tb_user` join `tb_teacher`) join `tb_title`) join `tb_depart`) join `tb_profess`) join `tb_title_profess`) where ((`tb_user`.`id` = `tb_teacher`.`userid`) and (`tb_depart`.`id` = `tb_title`.`depart_id`) and (`tb_title`.`teacher_id` = `tb_user`.`id`) and (`tb_title`.`teacher_id` = `tb_title_profess`.`title_id`) and (`tb_title_profess`.`profess_id` = `tb_profess`.`id`));
